import {DomElements} from './views/base'
import * as GameView from './views/GameView'
import * as GameUtils from './Util/GameUtils'

// Global App controller

const state = {
    correctWord:'',
    hiddenString:'____',
    wrongGuesses: [],
    gamePlaying: false
}


async function init() {
    state.gamePlaying=true;

    // Reset Title String
    GameView.resetTitle();

    // Generate Random Word
    state.correctWord = await GameUtils.getRandomWord();

    // Generate Hidden String
    state.hiddenString = GameUtils.generateHiddenString(state.correctWord);

    // Update UI with new hidden string
    GameView.updateHiddenString(state.hiddenString);
    
    // Change Start Button Text to New Game
    GameView.updateStartBtnText(state.gamePlaying);
    
    // Remove gray background from textbox and enter btn
    GameView.enableInputs();

}

// Checks if guess is correct or incorrect and delegates to appropriate method. 
const handleGuess = () => {

    // Get guess from UI
    const guess = GameView.getInput();
    // Validate input
    if(GameUtils.validateInput(guess, state.wrongGuesses)){
        // handle correct or incorrect guess.
        state.correctWord.includes(guess) ? correctGuess(guess) : wrongGuess(guess);
        // Check if the game is over.
        isGameOver();
    }

    // Clear input box
    GameView.clearInput();
}

const correctGuess = guess => {
    state.hiddenString = GameUtils.updateHiddenString(guess, state.hiddenString, state.correctWord);
    GameView.updateHiddenString(state.hiddenString);
}

const wrongGuess = guess => {
    state.wrongGuesses.push(guess);
    GameView.updateImage(state.wrongGuesses.length);
    GameView.updateWrongGuessLabel(state.wrongGuesses);
}


// Check if the game is over and update appropritatly
const isGameOver = () => {
    // Check if the player won.
    if(state.correctWord === state.hiddenString){
        state.gamePlaying = false;
        GameView.disableInputs();
        GameView.updateTitle('You Win!');
    };

    // Check if the player has lost.
    if(state.wrongGuesses.length === 6){
        state.gamePlaying = false;
        GameView.disableInputs();
        GameView.updateTitle('You Lose!');
    };

}


// Event Listeners
DomElements.startBtn.addEventListener('click', init);

DomElements.enterBtn.addEventListener('click', () => {
    if(state.gamePlaying){
        handleGuess();
    }
});

DomElements.inputBox.addEventListener('keypress', e => {
    if(e.keyCode === 13 && state.gamePlaying){
        handleGuess();
    }
})