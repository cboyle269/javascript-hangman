import axios from 'axios';

/*
TEMPLATE API CALL

https://api.wordnik.com/v4/words.json/randomWord
?hasDictionaryDef=true
&minCorpusCount=10000
&minDictionaryCount=1
&minLength=5&
maxLength=5
&api_key=b273ca761008e0355660f00bcde045e14a758d3de0df7f3ed
&includePartOfSpeech=noun,adjective,verb

*/

const apiKey= 'b273ca761008e0355660f00bcde045e14a758d3de0df7f3ed';


export async function getRandomWord() {
    const length = 5;
    const url = `https://api.wordnik.com/v4/words.json/randomWord`+
    `?hasDictionaryDef=true`+
    `&minCorpusCount=10000`+
    `&minDictionaryCount=1`+
    `&minLength=${length}`+
    `&maxLength=${length}`+
    `&api_key=${apiKey}`+
    `&includePartOfSpeech=noun,adjective,verb`

    try {
        const res = await axios(url);
        return res.data.word;
    }catch (ex) {
        alert(ex);
    }

}

export const generateHiddenString = correctWord => {
    const length = correctWord.length;
    let hiddenString = '';
    for(var i=0; i<length; i++){
        hiddenString= hiddenString + '_';
    }
    return hiddenString;
}

// Returns true if input is a letter and has not already been guessed. 
export const validateInput = (input, wrongGuesses) => {
    const isLetter = input.match(/[a-z]/i);
    const alreadyGuessed =  wrongGuesses.includes(input);

    return (isLetter && !alreadyGuessed)
}

export const updateHiddenString = (guess, hiddenString, correctWord) => {
    for(let i=0; i<hiddenString.length; i++){
        if(correctWord.charAt(i) === guess){
            hiddenString = replaceCharAt(guess, hiddenString, i);
        }
    }

    return hiddenString;
}

const replaceCharAt = (char, string, index) => {
    const start = string.substring(0, index);
    const end = string.substring(index+1);
    return start + char + end;
}