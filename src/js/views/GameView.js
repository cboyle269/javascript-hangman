import {DomElements} from './base'

export const resetTitle = () => {
    DomElements.titleLabel.textContent = 'Hangman';
};

export const updateTitle = newString => {
    DomElements.titleLabel.textContent = newString;
}

export const updateHiddenString = newString => {
    DomElements.hiddenString.textContent= newString;
}

export const updateStartBtnText = gamePlaying => {
    if(gamePlaying){
        DomElements.startBtn.textContent='New Game'
    }else{
        DomElements.startBtn.textContent='Start'
    }
}

export const enableInputs = () => {
    DomElements.inputBox.classList.remove('disabled');
    DomElements.enterBtn.classList.remove('disabled');
}

export const disableInputs = () => {
    DomElements.inputBox.classList.add('disabled');
    DomElements.enterBtn.classList.add('disabled');
}

export const getInput = () => {
    return DomElements.inputBox.value.toLowerCase();
}

export const clearInput = () => {
    DomElements.inputBox.value = '';
}

export const updateImage = number => {
    DomElements.hangmanImage.src = `./img/hangman-${number}.png`;
}

export const updateWrongGuessLabel = guesses => {
    DomElements.wrongLetters.textContent = guesses.join(' ,');
}