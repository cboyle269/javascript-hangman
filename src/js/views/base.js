export const DomElements = {
    titleLabel: document.querySelector('.title-label'),
    hangmanImage: document.querySelector(".hangman-img"),
    hiddenString: document.querySelector(".hidden-string-label"),
    inputBox: document.querySelector(".letter-input"),
    enterBtn: document.querySelector(".guess-btn"),
    startBtn: document.querySelector(".start-btn"),
    wrongLetters: document.querySelector(".wrong-letters-label")
};

