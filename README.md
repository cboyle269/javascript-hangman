# Javascript-Hangman

Simple hangman game created using JavaScript ES6, HTML and CSS 

---

## Installation instructions 
To build this project run the following npm command
```
    npm run build
```

To run the project using the following command 
```
    npm run serve
```

## Implementation notes
This project utilises the [Wordnik API](https://developer.wordnik.com) to generate a random word for each game.
Axios is used to make this API call. 

## Planned features
- [ ] loading spinner while API call is being made
- [ ] dropdown menu to select length of word 
- [ ] difficulty setting (Wordnik API courpusCount can be leveraged to return words of various commonality)